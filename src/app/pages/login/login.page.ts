import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { FormGroup, FormBuilder } from "@angular/forms";
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EventsService } from '../../services/events.service';
import { environment } from '../../../environments/environment';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  myForm: FormGroup;
  isSubmitted = false;
  chip_err: string;

  constructor(
    public events: EventsService,
    public firebaseX: FirebaseX,
    public formBuilder: FormBuilder,
    public http: HttpClient,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public storage: Storage,
  ) {
    this.myForm = this.formBuilder.group({
      no_hp: [''],
      password: ['']
    });
  }

  ngOnInit() {

  }

  async submit() {
    this.isSubmitted = true;

    if (this.myForm.valid) {
      const loading = await this.loadingController.create({});
      await loading.present();

      let url = environment.server + 'user_kurir/user_kurir_read_by_nohp.php';
      let body = new FormData();
      body.append('no_hp', this.myForm.get('no_hp').value);

      this.http.post(url, body).subscribe((data) => {
        if (data) {
          var bytes = CryptoJS.AES.decrypt(data[0].password, '');
          var plaintext = bytes.toString(CryptoJS.enc.Utf8);

          if (this.myForm.get('password').value == plaintext) {

            //GET FIREBASE TOKEN
            this.firebaseX.getToken().then(token => {
              let url_token = environment.server + 'user_kurir/user_kurir_update_firebasetoken.php';
              let body_firebase = new FormData();
              body_firebase.append('id', data[0].id);
              body_firebase.append('firebase_token', token);

              this.http.post(url_token, body_firebase).subscribe(() => {
                this.storage.set('profil', data[0]);
                this.events.pubAkun({});
                this.navCtrl.back();
              });
            }).catch(error => console.error('Error getting token', error));

            // this.navCtrl.navigateRoot('/tabs');

          } else {
            // alert('Password salah.');
            this.chip_err = 'password_salah';
          }
        } else {
          // alert('No. HP belum terdaftar.');
          this.chip_err = 'hp_salah';
        }

        loading.dismiss();
      }, error => {
        console.info(error.error);
        alert('Terjadi kesalahan. ' + JSON.stringify(error.error));

        loading.dismiss();
      });
    }
  }

}