import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController } from '@ionic/angular';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pesanan',
  templateUrl: './pesanan.page.html',
  styleUrls: ['./pesanan.page.scss'],
})
export class PesananPage implements OnInit {

  sgmStatus = 'pending';

  data_pending: any = [];
  data_dikirim: any = [];
  data_terkirim: any = [];
  data_batal: any = [];

  loading = true;

  empty_pending = true;
  empty_dikirim = true;
  empty_terkirim = true;
  empty_batal = true;

  constructor(
    public http: HttpClient,
    public loadingController: LoadingController,
    public storage: Storage,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(event) {
    this.load_data_pending(event);
    this.load_data_dikirim(event);
    this.load_data_terkirim(event);
    this.load_data_batal(event);
  }

  load_data_pending(event) {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=pending';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_pending = true;
        } else {
          this.data_pending = data;
          this.empty_pending = false;
        }

        this.loading = false;
        if (event) { event.target.complete(); }
      }, error => {
        console.info(error.error);
        this.load_data_pending(event);
        this.loading = false;
      });
    });
  }

  load_data_dikirim(event) {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=dikirim';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_dikirim = true;
        } else {
          this.data_dikirim = data;
          this.empty_dikirim = false;
        }

        this.loading = false;
        if (event) { event.target.complete(); }
      }, error => {
        console.info(error.error);
        this.load_data_dikirim(event);
        this.loading = false;
      });
    });
  }

  load_data_terkirim(event) {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=terkirim';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_terkirim = true;
        } else {
          this.data_terkirim = data;
          this.empty_terkirim = false;
        }

        this.loading = false;
        if (event) { event.target.complete(); }
      }, error => {
        console.info(error.error);
        this.load_data_terkirim(event);
        this.loading = false;
      });
    });
  }

  load_data_batal(event) {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=batal';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_batal = true;
        } else {
          this.data_batal = data;
          this.empty_batal = false;
        }

        this.loading = false;
        if (event) { event.target.complete(); }
      }, error => {
        console.info(error.error);
        this.load_data_batal(event);
        this.loading = false;
      });
    });
  }

  peta(val_lat, val_long) {
    // window.open('geo://' + val_lat + ', ' + val_long);
    window.open('geo://' + val_lat + ', ' + val_long + '?q=' + val_lat + ', ' + val_long, '_system');
  }

  async kirim(val_id) {
    const loading = await this.loadingController.create({});
    await loading.present();

    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_global_update_kurir_kirim.php';
      let body = new FormData();
      body.append('id', val_id);
      body.append('id_kurir', val.id);

      this.http.post(url, body).subscribe(async (data) => {
        console.info(data);
        this.load_data_pending(event);
        this.load_data_dikirim(event);
        loading.dismiss();

        const toast = await this.toastController.create({
          message: 'Segera ambil dan kirim pesanan.',
          position: 'top',
          color: 'light',
          duration: 1000,
          buttons: [
            {
              text: 'x',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();
      }, error => {
        console.info(error.error);
        loading.dismiss();
      });
    });
  }
}