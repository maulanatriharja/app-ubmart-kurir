import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events.service';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

import * as moment from 'moment';

@Component({
  selector: 'app-akun',
  templateUrl: './akun.page.html',
  styleUrls: ['./akun.page.scss'],
})
export class AkunPage implements OnInit {

  id: number;
  nama: string;
  no_hp: string;
  password: string;

  foto: string;

  constructor(
    public actionSheetController: ActionSheetController,
    public camera: Camera,
    public events: EventsService,
    public filePath: FilePath,
    public fileTransfer: FileTransfer,
    public http: HttpClient,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getProfil();

    this.events.obvAkun().subscribe((data) => {
      this.getProfil();
    })
  }

  getProfil() {
    this.storage.get('profil').then((val) => {
      this.id = val.id;
      this.nama = val.nama_kurir;
      this.no_hp = val.no_hp;
      this.password = val.password;

      this.foto = environment.server + 'user_kurir/foto/' + this.id + '.png?time=' + moment();
    });
  }

  async ubah_foto() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Photo',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          console.info('Open Camera');
          this.openCamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          console.info('Open Gallery');
          this.openGallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  openCamera() {
    let options = {
      allowEdit: true,
      correctOrientation: true,
      encodingType: this.camera.EncodingType.PNG,
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetHeight: 200,
      targetWidth: 200,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.filePath.resolveNativePath(imagePath).then(async filePath => {

        const loading = await this.loadingController.create({
          message: 'Uploading image...'
        });
        await loading.present();

        const fileTransfer: FileTransferObject = this.fileTransfer.create();
        const backend = environment.server + 'user_kurir/foto_upload.php';

        let options: FileUploadOptions = {
          fileKey: 'file',
          fileName: this.id + '.png',
          headers: { Accept: 'application/json', Authorization: 'Bearer ' }
        }

        fileTransfer.upload(filePath, backend, options).then((entry) => {
          this.foto = environment.server + 'user_kurir/foto/' + this.id + '.png?time=' + moment();
          loading.dismiss();
        }, (error) => {
          alert(JSON.stringify(error));
          loading.dismiss();
        });

      });
    }, (err) => {
      alert('Error while selecting image.');
      //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    });
  }

  openGallery() {
    let options = {
      allowEdit: true,
      correctOrientation: true,
      encodingType: this.camera.EncodingType.PNG,
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetHeight: 200,
      targetWidth: 200,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.filePath.resolveNativePath(imagePath).then(async filePath => {
        const loading = await this.loadingController.create({
          message: 'Uploading image...'
        });
        await loading.present();

        const fileTransfer: FileTransferObject = this.fileTransfer.create();
        const backend = environment.server + 'user_kurir/foto_upload.php';

        let options: FileUploadOptions = {
          fileKey: 'file',
          fileName: this.id + '.png',
          headers: { Accept: 'application/json', Authorization: 'Bearer ' }
        }

        fileTransfer.upload(filePath, backend, options).then((entry) => {
          this.foto = environment.server + 'user_kurir/foto/' + this.id + '.png?time=' + moment();
          loading.dismiss();
        }, (error) => {
          alert(JSON.stringify(error));
          loading.dismiss();
        });
      });
    }, (err) => {
      alert('Error while selecting image.');
      //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    });
  }

  ubah_nama() {
    this.router.navigate(['akun-nama'],
      {
        queryParams: {
          id: this.id,
          nama: this.nama
        }
      }
    );
  }

  ubah_nohp() {
    this.router.navigate(['akun-nohp'],
      {
        queryParams: {
          id: this.id,
          no_hp: this.no_hp
        }
      }
    );
  }

  ubah_password() {
    this.router.navigate(['akun-password'],
      {
        queryParams: {
          id: this.id,
          password: this.password
        }
      }
    );
  }

  keluar() {
    this.storage.clear();
    this.navCtrl.navigateRoot('/login');
  }
}