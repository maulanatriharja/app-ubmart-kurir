import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AkunNohpPageRoutingModule } from './akun-nohp-routing.module';

import { AkunNohpPage } from './akun-nohp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AkunNohpPageRoutingModule
  ],
  declarations: [AkunNohpPage]
})
export class AkunNohpPageModule {}
