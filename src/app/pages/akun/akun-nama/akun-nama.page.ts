import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { EventsService } from '../../../services/events.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-akun-nama',
  templateUrl: './akun-nama.page.html',
  styleUrls: ['./akun-nama.page.scss'],
})
export class AkunNamaPage implements OnInit {

  id: number;
  nama: string;

  proses_simpan: boolean = false;

  constructor(
    public events: EventsService,
    public navCtrl: NavController,
    public http: HttpClient,
    public route: ActivatedRoute,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.route.queryParams.subscribe(res => {
      this.id = res.id;
      this.nama = res.nama;
    })
  }

  simpan() {
    if (!this.nama) {
      alert('Silahkan isi nama anda.')
    } else {
      this.proses_simpan = true;

      let url = environment.server + 'user_kurir/user_kurir_update_nama.php';
      let body = new FormData();
      body.append('id', this.id.toString());
      body.append('nama_kurir', this.nama);

      this.http.post(url, body).subscribe((data) => {

        this.storage.get('profil').then((val) => {
          val.nama_kurir = this.nama;

          this.storage.set('profil', val).then(data => {
            this.events.pubAkun({})
            this.navCtrl.back();

            this.proses_simpan = false;
          });
        });

      }, error => {
        this.proses_simpan = false;
        console.info(error.error);
      });
    }
  }

}