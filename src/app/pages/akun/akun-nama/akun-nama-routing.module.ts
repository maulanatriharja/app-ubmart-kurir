import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AkunNamaPage } from './akun-nama.page';

const routes: Routes = [
  {
    path: '',
    component: AkunNamaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AkunNamaPageRoutingModule {}
