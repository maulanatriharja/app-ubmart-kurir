import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { EventsService } from '../../../services/events.service';
import { environment } from '../../../../environments/environment';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-akun-password',
  templateUrl: './akun-password.page.html',
  styleUrls: ['./akun-password.page.scss'],
})
export class AkunPasswordPage implements OnInit {

  id: number;
  password_aktif: string;

  myForm: FormGroup;
  isSubmitted = false;
  chip_err: string;

  plaintext: string;

  constructor(
    public events: EventsService,
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    public http: HttpClient,
    public loadingController: LoadingController,
    public route: ActivatedRoute,
    public storage: Storage,
  ) {
    this.myForm = this.formBuilder.group({
      password_lama: [''],
      password_baru: [''],
      password_konf: [''],
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.route.queryParams.subscribe(res => {
      this.id = res.id;
      this.password_aktif = res.password;
    })
  }

  async submit() {
    var bytes = CryptoJS.AES.decrypt(this.password_aktif, '');
    this.plaintext = bytes.toString(CryptoJS.enc.Utf8);

    this.isSubmitted = true;

    if (this.myForm.valid
      && this.myForm.get('password_lama').value == this.plaintext
      && this.myForm.get('password_baru').value != this.plaintext
      && this.myForm.get('password_baru').value == this.myForm.get('password_konf').value) {

      const loading = await this.loadingController.create({});
      loading.present();

      let url = environment.server + 'user_kurir/user_kurir_update_password.php';
      let body = new FormData();
      body.append('id', this.id.toString());
      body.append('password', CryptoJS.AES.encrypt(this.myForm.get('password_baru').value, '').toString());

      this.http.post(url, body).subscribe((data) => {

        this.storage.get('profil').then((val) => {
          val.password = CryptoJS.AES.encrypt(this.myForm.get('password_baru').value, '').toString();

          this.storage.set('profil', val).then(data => {
            this.events.pubAkun({})
            this.navCtrl.back();

            loading.dismiss();
          });
        });

      }, error => {
        loading.dismiss();
        console.info(error.error);
      });
    }
  }

}