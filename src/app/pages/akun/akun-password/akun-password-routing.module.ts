import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AkunPasswordPage } from './akun-password.page';

const routes: Routes = [
  {
    path: '',
    component: AkunPasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AkunPasswordPageRoutingModule {}
