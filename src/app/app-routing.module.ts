import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
    // loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registrasi',
    loadChildren: () => import('./pages/login/registrasi/registrasi.module').then( m => m.RegistrasiPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./pages/akun/akun.module').then( m => m.AkunPageModule)
  },
  {
    path: 'pesanan',
    loadChildren: () => import('./pages/pesanan/pesanan.module').then( m => m.PesananPageModule)
  }, 
  {
    path: 'akun-nama',
    loadChildren: () => import('./pages/akun/akun-nama/akun-nama.module').then( m => m.AkunNamaPageModule)
  },
  {
    path: 'akun-nohp',
    loadChildren: () => import('./pages/akun/akun-nohp/akun-nohp.module').then( m => m.AkunNohpPageModule)
  },
  {
    path: 'akun-password',
    loadChildren: () => import('./pages/akun/akun-password/akun-password.module').then( m => m.AkunPasswordPageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
