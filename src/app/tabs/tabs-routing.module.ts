import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'pesanan',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../pages/pesanan/pesanan.module').then(m => m.PesananPageModule)
          }
        ]
      },
      {
        path: 'akun',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../pages/akun/akun.module').then(m => m.AkunPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/pesanan',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/pesanan',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
