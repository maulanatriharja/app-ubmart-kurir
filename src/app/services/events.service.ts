import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  fooSubject = new Subject<any>();

  constructor() { }

  pubAkun(data: any) {  this.fooSubject.next(data);  }
  obvAkun(): Subject<any> {  return this.fooSubject; }
}
