import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { Camera } from '@ionic-native/camera/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AdMobPro } from '@ionic-native/admob-pro/ngx';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeId);
import localeId from '@angular/common/locales/id';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot({
      backButtonText: '',
      backButtonIcon: 'arrow-back',
      mode: 'ios'
    }),
    IonicStorageModule.forRoot(),
  ],
  providers: [
    { provide: FirestoreSettingsToken, useValue: {} },
    { provide: LOCALE_ID, useValue: 'id' },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AdMobPro,
    Camera,
    FilePath,
    FileTransfer,
    FirebaseX,
    StatusBar,
    SplashScreen,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
