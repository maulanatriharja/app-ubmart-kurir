// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server: 'https://ubmart.online/api_beta/',
  firebase: {
    apiKey: "AIzaSyBblmMJH7aqh0BI4lPvD4MndE_qitDqWtA",
    authDomain: "ubmart-online.firebaseapp.com",
    databaseURL: "https://ubmart-online.firebaseio.com",
    projectId: "ubmart-online",
    storageBucket: "ubmart-online.appspot.com",
    messagingSenderId: "967183193873",
    appId: "1:967183193873:web:dcfcacfe3c761927889eb0",
    measurementId: "G-5KRWYBRM6Q"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
